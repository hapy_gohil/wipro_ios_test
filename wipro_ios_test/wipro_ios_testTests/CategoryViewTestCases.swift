//
//  CategoryViewTestCases.swift
//  wipro_ios_testTests
//
//  Created by hapy gohil on 26/04/20.
//  Copyright © 2020 wipro. All rights reserved.
//

import UIKit
import XCTest
@testable import wipro_ios_test

class CategoryViewTestCases: XCTestCase {

    var controller: CategoryViewController!
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    
    override func setUp() {
        super.setUp()
        
        controller = CategoryViewController()
        controller.view.description
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    
    
    func testHasATableView() {
        XCTAssertNotNil(controller.tableView)
    }
    
    func testTableViewHasDelegate() {
        XCTAssertNotNil(controller.tableView.delegate)
    }
    
    func testTableViewConfromsToTableViewDelegateProtocol() {
        XCTAssertTrue(controller.conforms(to: UITableViewDelegate.self))
    }
    
    func testTableViewHasDataSource() {
        XCTAssertNotNil(controller.tableView.dataSource)
    }
    
    func testTableViewConformsToTableViewDataSourceProtocol() {
        XCTAssertTrue(controller.conforms(to: UITableViewDataSource.self))
        XCTAssertTrue(controller.responds(to: #selector(controller.tableView(_:numberOfRowsInSection:))))
        XCTAssertTrue(controller.responds(to: #selector(controller.tableView(_:cellForRowAt:))))
    }
    
}
