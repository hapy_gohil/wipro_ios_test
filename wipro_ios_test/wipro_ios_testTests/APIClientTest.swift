//
//  APIClientTest.swift
//  wipro_ios_testTests
//
//  Created by hapy gohil on 25/04/20.
//  Copyright © 2020 wipro. All rights reserved.
//

import XCTest
@testable import wipro_ios_test

class APIClientTest: XCTestCase {

    

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    
    
    
    
    var testAPI: APIClient?
    
    override func setUp() {
        super.setUp()
        testAPI = APIClient()
    }
    
    override func tearDown() {
        testAPI = nil
        super.tearDown()
    }
    
    func testNetwork()
    {
        
        if Reachability.isConnectedToNetwork() {
            XCTAssertTrue(Reachability.isConnectedToNetwork()  == true)
        }

    }
    func testAPIData() {
        guard let gitUrl = URL(string: "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json") else { return }
        let promise = expectation(description: "Simple Request")
        
        URLSession.shared.dataTask(with: gitUrl) { (data, response
            , error) in
            guard let data = data else { return }
            do {
                guard let string = String(data: data, encoding: String.Encoding.isoLatin1) else { return }
                guard let properData = string.data(using: .utf8, allowLossyConversion: true) else { return }
                
                let json = try JSONSerialization.jsonObject(with: properData, options: JSONSerialization.ReadingOptions.mutableContainers)
                if let result = json as? NSDictionary {
                    XCTAssertTrue(result["title"]  != nil)
                    XCTAssertTrue(result["rows"]  != nil)

                    promise.fulfill()
                }
            } catch let err {
                print("Error", err)
            }
            }.resume()
        waitForExpectations(timeout: 5, handler: nil)
    }


    func testAPICategoryData() {
        guard let gitUrl = URL(string: "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json") else { return }
        let promise = expectation(description: "Simple Request")
        
        URLSession.shared.dataTask(with: gitUrl) { (data, response
            , error) in
            guard let data = data else { return }
            do {
                guard let string = String(data: data, encoding: String.Encoding.isoLatin1) else { return }
                guard let properData = string.data(using: .utf8, allowLossyConversion: true) else { return }
                
                let json = try JSONSerialization.jsonObject(with: properData, options: JSONSerialization.ReadingOptions.mutableContainers)
                
                if let result = json as? NSDictionary {
                    
                    if let arr = result.object(forKey: "rows") as? NSArray
                    {
                        if arr.count > 0
                        {
                            if let firstObj = arr[0] as? NSDictionary
                            {
                                XCTAssertTrue(firstObj["title"]  != nil)
                                XCTAssertTrue(firstObj["description"]  != nil)
                                XCTAssertTrue(firstObj["imageHref"]  != nil)
                                

                            }
                        }
                        
                    }
                    
                    
                                        //XCTAssertTrue(result["rows"] as! String == "London")
                    promise.fulfill()
                }


                
            } catch let err {
                print("Error", err)
            }
            }.resume()
        waitForExpectations(timeout: 5, handler: nil)
    }

}

/*
enum Result<Value: Decodable> {
    case success(Value)
    case failure(Bool)
}
*/

