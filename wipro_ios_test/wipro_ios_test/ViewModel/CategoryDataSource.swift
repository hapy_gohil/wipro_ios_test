//
//  CategoryDataSource.swift
//  wipro_ios_test
//
//  Created by hapy gohil on 25/04/20.
//  Copyright © 2020 wipro. All rights reserved.
//

import Foundation
import UIKit


class CategoryDataSource: NSObject {
    
    
    weak var parentView: CategoryViewController?
    
    init(attachView: CategoryViewController) {
        super.init()
        self.parentView = attachView
        attachView.tableView.delegate = self
        attachView.tableView.dataSource = self
    }
    
}

// MARK: - UITableView Delegate And Datasource Methods

extension CategoryDataSource: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.parentView!.viewModel.Categorys!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : UITableViewCell!
        cell = tableView.dequeueReusableCell(withIdentifier: "CategoryViewCell")
        if cell == nil {
            cell = UITableViewCell(style: .default, reuseIdentifier: "CategoryViewCell")
        }
        cell.textLabel?.text = self.parentView!.viewModel.Categorys![indexPath.row].title ?? ""
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}

