//
//  Categories.swift
//  wipro_ios_test
//
//  Created by hapy gohil on 24/04/20.
//  Copyright © 2020 wipro. All rights reserved.
//

import Foundation

// this is category response root keys
struct CategoryResponseModel : Codable {
    
    let Maintitle : String?
    let rows : [Category]?
    
    enum CodingKeys: String, CodingKey {
        
        case Maintitle = "title"
        case rows = "rows"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        Maintitle = try values.decodeIfPresent(String.self, forKey: .Maintitle)
        rows = try values.decodeIfPresent([Category].self, forKey: .rows)
    }
    
}

// this is category response inside rows , this save as Categories
struct Category : Codable {
    let title : String?
    let description : String?
    let imageHref : String?
    
    enum CodingKeys: String, CodingKey {
        
        case title = "title"
        case description = "description"
        case imageHref = "imageHref"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        imageHref = try values.decodeIfPresent(String.self, forKey: .imageHref)
    }
    
}
