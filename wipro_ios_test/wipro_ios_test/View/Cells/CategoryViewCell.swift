//
//  CategoryViewCell.swift
//  wipro_ios_test
//
//  Created by hapy gohil on 25/04/20.
//  Copyright © 2020 wipro. All rights reserved.
//

import UIKit
import SDWebImage
class CategoryViewCell: UITableViewCell {
    
    //@IBOutlet weak var picture: UIImageView!
    
    //@IBOutlet weak var titleLabel: UILabel!
    
    //@IBOutlet weak var overviewLabel: UILabel!
    
    // here we declare imageview and lable for display , title description and image
    let picture:UIImageView = {
        let imgView = UIImageView(image: #imageLiteral(resourceName: "pla"))
        imgView.contentMode = .scaleAspectFill
        imgView.clipsToBounds = true
        return imgView
    }()
    
    // this we declare title  label
    let titleLabel:UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.boldSystemFont(ofSize: Device().checkSize(sizes: 16))
        lbl.textAlignment = .left
        return lbl
    }()
    
    
    let overviewLabel:UILabel = {
        
        let label = UILabel()
        label.textColor = .black
        label.font = UIFont.systemFont(ofSize: Device().checkSize(sizes:  14))
        label.textAlignment = .left
        label.numberOfLines = 0

        
        return label
    }()

    
    // from cellforrowatindex this category items is set
    // from tableviewcell category item is filled by arrayof category data
    var CategoryItem: Category? {
        
        didSet {
            
            if let Category = CategoryItem {
                
                //self.awakeFromNib()
                
                self.loadUI(cat: Category)
                self.titleLabel.text = ""
                self.overviewLabel.text = ""
                
                self.titleLabel.text = Category.title ?? ""
                
                self.overviewLabel.text = Category.description ?? ""
                
                // here we set image using sdwebimage third party library
                if Category.imageHref == nil || Category.imageHref == ""
                {
                    self.picture.image = UIImage.init(named: "pla")
                }
                else
                {
                    self.picture.sd_imageIndicator = SDWebImageActivityIndicator.gray
                    self.picture.sd_setImage(with: URL.init(string: Category.imageHref ?? ""), placeholderImage: UIImage.init(named: "pla"))
                }
                
            }
        }
    }
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

    
    
    // From this method we added constraint to cell and setup constraint for titlelable , picture and overviewlabel
    func loadUI(cat : Category)
    {
        picture.removeFromSuperview()
        titleLabel.removeFromSuperview()
        overviewLabel.removeFromSuperview()
        
        
        addSubview(titleLabel)
        addSubview(overviewLabel)
        addSubview(picture)
        
        titleLabel.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 5, paddingLeft: Device().checkSize(sizes: 60), paddingBottom: 0, paddingRight: 0, width: frame.size.width / 2, height: 0, enableInsets: false)
        overviewLabel.anchor(top: titleLabel.bottomAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 0, paddingLeft: Device().checkSize(sizes: 60), paddingBottom: 0, paddingRight: 0, width: frame.size.width / 2, height: 0, enableInsets: false)
        picture.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 5, paddingLeft: 5, paddingBottom: 5, paddingRight: 0, width: Device().checkSize(sizes:  50), height: Device().checkSize(sizes: 50), enableInsets: false)
        
    
        let rowRight = self.estimatedLabelHeight(text: cat.description ?? "", width: self.contentView.frame.width - 50, font: UIFont.systemFont(ofSize: Device().checkSize(sizes:  14)))
        
        let stackView = UIStackView(arrangedSubviews: [])
        stackView.distribution = .equalSpacing
        stackView.axis = .horizontal
        stackView.spacing = 5
        addSubview(stackView)
        stackView.anchor(top: topAnchor, left: overviewLabel.rightAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 15, paddingLeft: 5, paddingBottom: 15, paddingRight: 10, width: 0, height: rowRight + 50, enableInsets: false)
        
    }
    
    
    func estimatedLabelHeight(text: String, width: CGFloat, font: UIFont) -> CGFloat {
        
        let size = CGSize(width: width, height: 1000)
        
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        
        let attributes = [NSAttributedString.Key.font: font]
        
        let rectangleHeight = String(text).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
        
        return rectangleHeight
    }

    
}

