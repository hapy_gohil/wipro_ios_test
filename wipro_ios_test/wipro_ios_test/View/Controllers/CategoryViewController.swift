//
//  CategoryViewController.swift
//  wipro_ios_test
//
//  Created by hapy gohil on 24/04/20.
//  Copyright © 2020 wipro. All rights reserved.
//

import UIKit

class CategoryViewController: UIViewController , ShowsAlert{
    
    //MARK: Internal Properties
    var refreshControl = UIRefreshControl()
    let tableView = UITableView(frame: .zero, style: .plain)
    var stackView: UIStackView {
        let stackView = UIStackView(frame: .zero)
        stackView.axis = .vertical
        return stackView
    }
    
    
    // this is view model class , using it we bind api data with table
    let viewModel = CategoryViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // first check internet is available or not
        if Reachability.isConnectedToNetwork(){
            print("Internet Connection Available!")
        }else{
            print("Internet Connection not Available!")
            showAlert(title: "Wipro_test", message: "Internet Connection not Available! ")
        }

        
        // Do any additional setup after loading the view.
        prepareUI()
        setData()
        fetchCategoryList()
    }
    
    func setData() {
        self.navigationItem.title = viewModel.navTitle
    }
    
}

//MARK: Prepare UI

extension CategoryViewController {
    
    func prepareUI() {
        prepareTableView()
        prepareStackView()
        prepareViewModelObserver()
    }
    
    // add stackview in view
    func prepareStackView() {
        let stackView = UIStackView(arrangedSubviews: [tableView])
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.alignment = .fill
        stackView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(stackView)
        stackView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0.0).isActive = true
        stackView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0.0).isActive = true
        stackView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0.0).isActive = true
        stackView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0.0).isActive = true
    }
    
    
    // add tableview programatically in view
    func prepareTableView() {
        self.view.backgroundColor = .white
        self.tableView.separatorStyle   = .none
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.register(CategoryViewCell.self, forCellReuseIdentifier: "CategoryViewCell")
        
        // add refresh control to the table
        refreshControl.attributedTitle = NSAttributedString(string: "Loading..")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tableView.addSubview(refreshControl) // not required when using UITableViewController

    }
    
    
    
    // after pull to refresh this method call , using it you able to fetch data from api
    @objc func refresh(_ sender: AnyObject) {
        // Code to refresh table view
        self.fetchCategoryList()
    }

    
}


//MARK: Private Methods
// this is methods for fetch data from api using view model file and also update tableview
extension CategoryViewController {
    
    func fetchCategoryList() {
        viewModel.fetchCategoryList()
        
    }
    
    func prepareViewModelObserver() {
        self.viewModel.CategoryDidChanges = { (finished, error) in
            if !error {
                self.reloadTableView()
            }
        }
    }
    
    func reloadTableView() {
        self.navigationItem.title = viewModel.navTitle
        self.tableView.reloadData()
        refreshControl.endRefreshing()
    }
}

// MARK: - UITableView Delegate And Datasource Methods

extension CategoryViewController: UITableViewDelegate, UITableViewDataSource {
    
    // set number of row to table
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.Categorys!.count
    }
    
    // this method is setup cell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell: CategoryViewCell = tableView.dequeueReusableCell(withIdentifier: "CategoryViewCell", for: indexPath as IndexPath) as? CategoryViewCell else {
            fatalError("AddressCell cell is not found")
        }
        
        let Category = viewModel.Categorys![indexPath.row]
        cell.CategoryItem = Category
        
        return cell
    }
    
    // this method is setup height of the row
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let catObj = viewModel.Categorys![indexPath.row]
        
        var heightRow = self.estimatedLabelHeight(text: catObj.description ?? "", width: 200, font: UIFont.systemFont(ofSize: 14))
        
        if heightRow < minHeihgt()
        {
            heightRow = minHeihgt()
        }
        return heightRow + (minHeihgt() / 3)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        let catObj = viewModel.Categorys![indexPath.row]
        
        var heightRow = self.estimatedLabelHeight(text: catObj.description ?? "", width: 200, font: UIFont.systemFont(ofSize: 14))
        
        
        if heightRow < minHeihgt()
        {
            heightRow = minHeihgt()
        }
        return heightRow + (minHeihgt() / 3)
    }
    
    
    // this method is finds height of desciption
    func estimatedLabelHeight(text: String, width: CGFloat, font: UIFont) -> CGFloat {
        
        let size = CGSize(width: width, height: 1000)
        
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        
        let attributes = [NSAttributedString.Key.font: font]
        
        let rectangleHeight = String(text).boundingRect(with: size, options: options, attributes: attributes, context: nil).height
        
        return rectangleHeight
    }

    // as per different height for iphone and ipad this method is set minimum Height of row
    func minHeihgt() -> CGFloat
    {
        if Device.IS_IPAD == true
        {
            return 100
        }
        else
        {
            return 50
        }
    }
}
